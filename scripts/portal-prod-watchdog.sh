#!/bin/bash
SCRIPTDIR="/data/scripts/admin"

if [ `whoami` != "root" ]; then
  echo "Must be root (or sudo)!"; echo
  exit 1
fi

#if [ -d $SCRIPTDIR ]; then
#  cd $SCRIPTDIR
#  su portaladmin -c "./portal-prod-local-cmd.sh status > /tmp/portal-status 2>&1"
#  grep -i "not running" /tmp/portal-status > /dev/null 2>&1

#  if [ $? -eq 0 ]; then

#    echo "DEBUG: Portal watchdog discovered portal not running, attempting to start now."

#    logger -t PORTAL "Portal watchdog discovered portal not running, attempting to start now."
    ## Not running, attempt re-deployment
    #su portaladmin -c "export USER=\"portaladmin\"; ./portal-admin -c QFAB-local $INSTANCE start > /dev/null 2>&1"

#    su portaladmin -c "./portal-prod-local-cmd.sh start"

  ### DEBUG ###
  #else
  #   echo "DEBUG: Portal running, watchdog takes no action."
#  fi
#fi


check_restart_postgres () {
  ## Make sure postgres is running before we fire up pylons.
  ## If it isn't, fire it up.
  /bin/systemctl status postgresql > /tmp/postgresql-local-status 2>&1
  grep -i "running" /tmp/postgresql-local-status > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo "DEBUG: Portal Prod watchdog discovered postgresql-local not running, attempting to start now."
    logger -t PORTAL "Portal Prod watchdog discovered postgresql-local not running, attempting to start now."
    /bin/systemctl start postgresql
    ## Postgres did not restart, quit
    if [ $? -ne 0 ]; then
      echo "DEBUG: postgresql-local could not be restarted, abandoning attempt to restart Portal Prod."
      logger -t PORTAL "postgresql-local could not be restarted, abandoning attempt to restart Portal Prod."
      exit 1
    fi
  fi
}

check_restart_redis () {
  /bin/systemctl status redis > /tmp/redis-server-status 2>&1
  grep -i "running" /tmp/redis-server-status > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo "DEBUG: Portal Prod watchdog discovered redis-server not running, attempting to start now."
    logger -t PORTAL "Portal Prod watchdog discovered redis-server not running, attempting to start now."
    /bin/systemctl start redis
    chmod 777 /tmp/redis.sock
    ## Postgres did not restart, quit
    if [ $? -ne 0 ]; then
      echo "DEBUG: redis-server could not be restarted, abandoning attempt to restart Portal Prod."
      logger -t PORTAL "redis-server could not be restarted, abandoning attempt to restart Portal Prod."
      exit 1
    fi
  fi

  ## Task #705 - Independent check that redis is running according to TCP connectivity,
  ## as sometimes "running" status is falsely reported
  #netstat -an | grep -P "0\.0\.0\.0\:6379.*LISTEN" > /dev/null 2>&1
  #if [ $? -ne 0 ]; then
  #  echo "DEBUG: Portal Prod watchdog discovered redis-server not running on port 6379, attempting to (re)start now."
  #  logger -t PORTAL "Portal Prod watchdog discovered redis-server not running on port 6379, attempting to (re)start now."
  #  /etc/init.d/redis-server restart
  #fi
}

check_restart_httpd () {
  /bin/systemctl status httpd> /tmp/httpd-server-status 2>&1
  grep -i "running" /tmp/httpd-server-status > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo "DEBUG: Portal Prod watchdog discovered httpd-server not running, attempting to start now."
    logger -t PORTAL "Portal Prod watchdog discovered httpd-server not running, attempting to start now."
    /bin/systemctl start httpd
    ## Postgres did not restart, quit
    if [ $? -ne 0 ]; then
      echo "DEBUG: httpd-server could not be restarted, abandoning attempt to restart Portal Prod."
      logger -t PORTAL "httpd-server could not be restarted, abandoning attempt to restart Portal Prod."
      exit 1
    fi
  fi

}



### MAIN ###

if [ -d $SCRIPTDIR ]; then
  cd $SCRIPTDIR
  
  # check the dependencies first before pylons. Pylons usually gets started last
  check_restart_postgres
  check_restart_redis
  check_restart_httpd

  su portaladmin -c "./portal-prod-local-cmd.sh status > /tmp/portal-prod-status 2>&1"
  grep -i "not running" /tmp/portal-prod-status > /dev/null 2>&1

  if [ $? -eq 0 ]; then
    echo "DEBUG: Portal Prod watchdog discovered pylons not running, will attempt restart."
    logger -t PORTAL "Portal Prod watchdog discovered pylons not running, will attempt restart."


    sleep 5
    ## Not running, attempt pylons re-deployment
    su portaladmin -c "./portal-prod-local-cmd.sh start"
  fi
  ### DEBUG ###
  #else
  #   echo "DEBUG: Portal running, watchdog takes no action."
  
fi

