#!/bin/bash
# example command - ./create_database_entry_for_empty_dataset.sh 1111 'title' 'description' 'handle' db_id

ds_id=$1
title=$2
description=$3
handle=$4
db_id=$5

echo "======================================================"
echo -e "You have entered - id = $ds_id \nTitle = $title \nDescription = $description \nhandle = $handle \ndb_id = $db_id "
echo  "======================================================"
echo -e "Please press 1 to create entries in database for datasets and dataset_metadata \nPress 2 to exit and re-enter values \nPress 3 for help"
read answer
if [ $answer -eq 1 ]
then
    cmd=''
    echo "For Dataset is -  $ds_id"
    echo "============================"

    lab='unknown'
    published=true
    private=true
    chip_type=0
    min_y_axis=0
    show_yugene=false
    show_limited=false
    number_of_samples=0
    data_type_id=0
    mapping_id=0
    log_2=false
    cmd+="INSERT INTO DATASETS values ($ds_id,'$lab','$(date +%d-%m-%Y" "%H:%M:%S)','$handle',$published,$private, $chip_type, $min_y_axis, $show_yugene, $show_limited,$db_id,$number_of_samples,$data_type_id,$mapping_id,$log_2);"
    cmd+="INSERT INTO dataset_metadata (ds_id,ds_name,ds_value) VALUES ($ds_id , 'has_data' , 'no');"
    cmd+="INSERT INTO dataset_metadata (ds_id,ds_name,ds_value) VALUES ($ds_id , 'Title' , '$title');"
    cmd+="INSERT INTO dataset_metadata (ds_id,ds_name,ds_value) VALUES ($ds_id , 'Description' , '$description');"
    echo $cmd
    psql -U portaladmin portal_prod -c " ${cmd}"
    echo "All done"
    echo "======================"
elif [ $answer -eq 2 ]
then
    echo "Exiting"
    exit 0
elif [ $answer -eq 3 ]
then
    echo -e "\nExample Command "
    echo "./create_database_entry_for_empty_dataset.sh dataest_id 'your_title' 'very very long description' 'unique_handle' db_id "
else
    echo "Exiting"
    exit 0
fi
