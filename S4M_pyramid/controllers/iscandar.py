import logging
import os

import psycopg2
import psycopg2.extras

from S4M_pyramid.lib.JsonValueInterceptor import JsonValueInterceptor
from S4M_pyramid.lib.deprecated_pylons_abort_and_redirect import redirect
from S4M_pyramid.model.stemformatics import Stemformatics_Dataset, Stemformatics_Gene

log = logging.getLogger(__name__)

from pyramid_handlers import action
from S4M_pyramid.lib.base import BaseController
from S4M_pyramid.lib.deprecated_pylons_globals import config, url
from pyramid.renderers import render_to_response

from S4M_pyramid.model.stemformatics import db_deprecated_pylons_orm as db

class GeneInfo(object):
    """
    Interface to define structure of gene info
    """
    def __init__(self, gene_id, associated_gene_name):
        self.gene_id = gene_id
        self.associated_gene_name = associated_gene_name

class IscandarController(BaseController):
    gene_autocomplete_limit = 20
    # base directory with iscandar data files
    data_dir = '/var/www/pylons-data/prod/singleCellFiles/'
    pretty_file_name_suffix = '.3f'

    def __init__(self, request):
        """
        May be usefull
        """
        super().__init__(request)
        c = self.request.c
        self.human_db = config['human_db']
        self.mouse_db = config['mouse_db']
        self.default_human_dataset = config['default_human_dataset']
        self.default_mouse_dataset = config['default_mouse_dataset']
        c.human_db = self.human_db
        c.mouse_db = self.mouse_db

    def plot(self):
        """
        Plot action
        """
        c = self.request.c  # I don't why, but this helps to pass variables into template
        request = self.request
        c.ds_id = request.params.get("ds_id")
        if not c.ds_id or not self._have_access(c.ds_id, c.uid):
            return redirect(url(controller='contents', action='index'), code=404)

        dict_of_ds_ids = {}
        dict_of_ds_ids[int(c.ds_id)] = {'dataset_status': Stemformatics_Dataset.check_dataset_with_limitations(db, c.ds_id, c.uid)}
        format_type = 'front_end'
        c.dataset = Stemformatics_Dataset.get_dataset_metadata(dict_of_ds_ids, format_type)
        c.dataset[c.ds_id] = c.dataset[int(c.ds_id)]
        c.db_id = Stemformatics_Dataset.get_db_id(c.ds_id)
        c.model_info = {  # some information about possible plot settings
            'sample_groups': self._get_file_names_in_dir(c.ds_id, 'sampleGroups'),  # list of sample groups
            'clusters': self._get_file_names_in_dir(c.ds_id, 'clusters'),  # list of clusters
            'genesets': self._get_file_names_in_dir(c.ds_id, 'geneSets'),  # list of genesets
        }
        # load configuration form url or use default values
        c.plotType = request.params.get("plotType") or 'pca'
        c.plotBy = request.params.get("plotBy") or 'sample_group'
        c.plotByX = request.params.get("plotByX") or 'gene'
        c.plotByY = request.params.get("plotByY") or 'gene'
        c.geneset = self._get_value_from_url_or_first_available("geneset", c.model_info['genesets'])
        c.gene = self._get_gene_data(request.params.get("gene"))
        c.geneX = self._get_gene_data(request.params.get("geneX"))
        c.geneY = self._get_gene_data(request.params.get("geneY"))
        if not c.geneX.gene_id and not c.geneY.gene_id:
            genes = Stemformatics_Gene.search_genes_by_pattern('', c.db_id, c.dataset[c.ds_id]['mapping_id'], 2)
            if len(genes) == 2:
                c.geneY = GeneInfo(genes[1]['gene_id'], genes[1]['associated_gene_name'])
                c.geneX = GeneInfo(genes[0]['gene_id'], genes[0]['associated_gene_name'])
        c.geneSet = request.params.get("geneSet")
        c.geneSetX = request.params.get("geneSetX")
        c.geneSetY = request.params.get("geneSetY")
        c.sample_group = self._get_value_from_url_or_first_available("sampleGroup", c.model_info['sample_groups'])
        c.cluster = self._get_value_from_url_or_first_available("cluster", c.model_info['clusters'])

        return render_to_response("S4M_pyramid:templates/iscandar/plot.mako", self.deprecated_pylons_data_for_view,
                                  request=self.request)

    def _get_gene_data(self, gene_id):
        if not gene_id:
            return GeneInfo(None, None)
        gene = Stemformatics_Gene.get_gene(gene_id)
        return GeneInfo(gene['gene_id'], gene['associated_gene_name'])

    @action(renderer="string")
    def graph_data(self):
        """
        Loads necessary data according url params
        """
        params = self.request.params
        error = None
        resp_data = dict()
        pretty = True
        if self.request.params.get("pretty"):  # set any value to download data without rounding
            pretty = False

        ds_id = self.request.params.get("ds_id")
        if not ds_id or not self._have_access(ds_id, self.request.c.uid):
            return redirect(url(controller='contents', action='index'), code=404)

        lazy_json = JsonValueInterceptor()

        if params.get("sampleIds"):
            resp_data['sampleIds'] = lazy_json.register_value(self._get_sample_ids(ds_id))

        if params.get("pca"):
            resp_data['pca'] = lazy_json.register_value(self._get_pca(ds_id, 'pca', pretty))

        if params.get("tsne"):
            resp_data['tsne'] = lazy_json.register_value(self._get_pca(ds_id, 'tsne', pretty))

        if params.get("sampleGroup"):
            sample_groups = params.get("sampleGroup").split(',')
            resp_data['sampleGroup'] = {}
            for sample_group in sample_groups:
                resp_data['sampleGroup'][sample_group] = lazy_json.register_value(
                    self._get_sample_group(ds_id, sample_group))

        if params.get("cluster"):
            clusters = params.get("cluster").split(',')
            resp_data['cluster'] = {}
            for cluster in clusters:
                resp_data['cluster'][cluster] = lazy_json.register_value(
                    self._get_cluster(ds_id, cluster))

        if params.get("geneSet"):
            gene_sets = params.get("geneSet").split(',')
            resp_data['geneSet'] = {}
            for gene_set in gene_sets:
                resp_data['geneSet'][gene_set] = lazy_json.register_value(self._get_gene_set(ds_id, gene_set, pretty))

        return lazy_json.json_dump({"data": resp_data, "error": error})

    def _get_value_from_url_or_first_available(self, key, available_values):
        """
        Returns request value or the first element of list or empty string if list is empty
        :param key: string
        :param available_values: list|None
        :return: string
        """
        return self.request.params.get(key) or next(iter(available_values or []), '')

    def _get_file_names_in_dir(self, ds_id, dir):
        """
        Returns unique file names in folder
        :param ds_id: string
        :param dir: string
        :return: list(string)
        """
        file_names = []
        json_len = len('.json')
        json_3f = self.pretty_file_name_suffix + '.json'
        json_3f_len = len(json_3f)
        for file in os.listdir(os.path.join(self.data_dir, ds_id, dir)):
            if file[-json_3f_len:] != json_3f:
                # filter duplicates with names '.3f.json'
                file_names.append(file[:-json_len])
        return file_names

    def _get_pca(self, ds_id, pca, pretty=True):
        file_name = pca
        if pretty:
            file_name += self.pretty_file_name_suffix
        file_name += '.json'  # tsne.3f.json
        file_path = os.path.join(self.data_dir, ds_id, file_name)
        return open(file_path, 'r').read()

    def _get_gene_set(self, ds_id, gene_set, pretty=True):
        gene_set_file_name = gene_set
        if pretty:
            gene_set_file_name += self.pretty_file_name_suffix
        gene_set_file_name += '.json'
        return open(os.path.join(self.data_dir, ds_id, 'geneSets', gene_set_file_name)).read()

    def _get_sample_ids(self, ds_id):
        return open(os.path.join(self.data_dir, ds_id, 'sampleIds.json')).read()

    def _get_sample_group(self, ds_id, sample_group):
        return open(os.path.join(self.data_dir, ds_id, 'sampleGroups', sample_group + '.json')).read()

    def _get_cluster(self, ds_id, cluster):
        return open(os.path.join(self.data_dir, ds_id, 'cluster', cluster + '.json')).read()

    def _have_access(self, ds_id, uid):
        status = Stemformatics_Dataset.check_dataset_with_limitations(db, ds_id, uid)
        return status == 'Available'

