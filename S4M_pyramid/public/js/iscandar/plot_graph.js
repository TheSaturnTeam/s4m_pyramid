(function () {
    /**
     * ===================
     * DEFINE VARIABLES
     * ===================
     */
    /** Some constants */
    const PCA = 'pca';
    const TSNE = 'tsne';
    const PCA_AND_TSNE = 'pca_tsne';
    const SAMPLE_GROUP = 'sampleGroup';
    const CLUSTER = 'cluster';
    const GENE = 'gene';
    const GENE_SET = 'geneSet';
    const GENE_VS_GENE = 'gene_vs_gene';

    const loader = new Loader(); // loader animation
    /**
     * All downloaded data from stemformatics
     * @type {{
     * sampleIds: string[],
      * pca: {x: number[], y: number[]},
      * tsne: {x: number[], y: number[]},
      * sampleGroup: Map<string, number[]>,
      * gene: Map<string, Map<string, number|null>,
      * cluster: Map<string, number[]>,
      * geneSet: Map<string, number[]>,
      * }}
     */
    const cache = {
        sampleIds: null,
        pca: null,
        tsne: null,
        sampleGroup: {},
        gene: {},
        cluster: {},
        geneSet: {},
    };

    /** Plotly layout settings */
    const layout = {
        height: 500,
        hovermode: 'closest',
        width: 1000,
        images: [
            {
                x: 1.1,
                y: 0.8,
                sizex: 0.6,
                sizey: 0.6,
                source: "/img/logo90.png",
                xanchor: "right",
                xref: "paper",
                yanchor: "top",
                yref: "paper",
            },
        ],
        margin: {
            t: 50,
        },
        showlegend: true,
        legend: {
            x: 1.1,
            y: 1,
        },
    };
    /** HTMLInputElement and HTMLSelectElements */
    const plotTypeElement = document.getElementById('plotType');
    const plotByElement = document.getElementById('plotBy');
    const plotByXElement = document.getElementById('plotByX');
    const plotByYElement = document.getElementById('plotByY');
    const sampleGroupElement = document.getElementById('sampleGroup');
    const clusterElement = document.getElementById('cluster');
    const geneSymbolElement = document.getElementById('gene');
    const geneSymbolXElement = document.getElementById('geneX');
    const geneSymbolYElement = document.getElementById('geneY');
    const geneIdElement = document.getElementById('geneId');
    const geneIdXElement = document.getElementById('geneIdX');
    const geneIdYElement = document.getElementById('geneIdY');
    const geneSetElement = document.getElementById('geneSet');
    const geneSetXElement = document.getElementById('geneSetX');
    const geneSetYElement = document.getElementById('geneSetY');

    /** Current configuration to plot */
        // let plotBy = plotByElement.value;
    const plotBy = new PlotBy(plotByElement);
    const plotByX = new PlotByAuxiliary(plotByXElement);
    const plotByY = new PlotByAuxiliary(plotByYElement);
    let plotType = plotTypeElement.value;
    let sampleGroup = sampleGroupElement.value;
    let cluster = clusterElement.value;


    let gene = {value: geneIdElement.value, name: geneSymbolElement.value};
    let geneX = {value: geneIdXElement.value, name: geneSymbolXElement.value};
    let geneY = {value: geneIdYElement.value, name: geneSymbolYElement.value};
    let geneSet = geneSetElement.value;
    let geneSetX = geneSetXElement.value;
    let geneSetY = geneSetYElement.value;
    let dbId = document.getElementById('db_id').innerHTML.trim(); // human or mouse database
    let dataSetId = document.getElementById('ds_id').innerHTML.trim(); // selected dataset id

    /** Check data from html */
    if (!dataSetId) {
        throw 'Data set is not defined'
    }
    const geneChange = paramChanged(function (value) {
        gene = value
    });
    const geneXChange = paramChanged(function (value) {
        geneX = value
    });
    const geneYChange = paramChanged(function (value) {
        geneY = value
    });
    /**
     * ===================
     * DEFINE ON CHANGE HANDLERS
     * ===================
     */
    plotTypeElement.onchange = paramChanged(function (event) {
        plotType = event.target.value;
        toggleControlsVisibility();
    });
    sampleGroupElement.onchange = paramChanged(function (event) {
        sampleGroup = event.target.value
    });
    clusterElement.onchange = paramChanged(function (event) {
        cluster = event.target.value
    });
    geneSetElement.onchange = paramChanged(function (event) {
        geneSet = event.target.value
    });
    geneSetXElement.onchange = paramChanged(function (event) {
        geneSetX = event.target.value
    });
    geneSetYElement.onchange = paramChanged(function (event) {
        geneSetY = event.target.value
    });
    set_gene_search_and_autocomplete(document.getElementById('gene'), geneChange); // gene on change
    set_gene_search_and_autocomplete(document.getElementById('geneX'), geneXChange); // gene on change
    set_gene_search_and_autocomplete(document.getElementById('geneY'), geneYChange); // gene on change

    /**
     * ===================
     * DEFINES NON-PLOT FUNCTIONS
     * ===================
     */

    function PlotBy(element) {
        const self = this;
        this.bySampleGroup = function () {
            return this._value === SAMPLE_GROUP
        };
        this.byCluster = function () {
            return this._value === CLUSTER
        };
        this.byGene = function () {
            return this._value === GENE
        };
        this.byGeneSet = function () {
            return this._value === GENE_SET
        };
        this.value = function (value) {
            if (value) {
                if ([SAMPLE_GROUP, CLUSTER, GENE, GENE_SET].indexOf(value) === -1) {
                    throw 'Something is totally wrong'
                }
                this._value = value
            }
            else {
                return this._value
            }
        };
        this.value(element.value);
        element.onchange = paramChanged(function (event) {
            self.value(event.target.value)
            toggleControlsVisibility();
        });
    }

    function PlotByAuxiliary(element) {
        const self = this;
        this.byGene = function () {
            return this._value === GENE
        };
        this.byGeneSet = function () {
            return this._value === GENE_SET
        };
        this.value = function (value) {
            if (value) {
                if ([GENE, GENE_SET].indexOf(value) === -1) {
                    throw 'Something is totally wrong'
                }
                this._value = value
            }
            else {
                return this._value
            }
        };
        this.value(element.value);
        element.onchange = paramChanged(function (event) {
            self._value = event.target.value;
            toggleControlsVisibility();
        });
    }

    /**
     * Loading animation. Supports waiting for several queries
     * @constructor
     */
    function Loader() {
        this._counter = 0;
        this.show = function () {
            if (this._counter === 0) {
                document.getElementById('loading').style.display = 'block';
            }
            this._counter++;
        };
        this.hide = function () {
            this._counter--;
            if (this._counter === 0) {
                document.getElementById('loading').style.display = 'none';
            }
        };
        this.isHidden = function () {
            return this._counter === 0;
        };
    }

    function set_gene_search_and_autocomplete(geneElement, geneChange) {
        let AUTOCOMPLETE_TIMEOUT = 10000;
        $(geneElement).autocomplete({
            source: '/genes/get_autocomplete_with_mapping?ds_id=' + dataSetId,
            minLength: 2, // because the names are very small eg. STAT1
            timeout: AUTOCOMPLETE_TIMEOUT,
            appendTo: ".searchBox",
            select: function (event, ui) {
                event.preventDefault();
                geneElement.value = ui.item.associated_gene_name;
                geneChange({
                    value: ui.item.gene_id,
                    name: ui.item.associated_gene_name,
                });
            },
            response: function (event, ui) {
                ui.content.some(function (geneInfo) {
                    if (geneInfo.associated_gene_name.toUpperCase() === geneElement.value.toUpperCase()) {
                        geneChange({
                            value: geneInfo.gene_id,
                            name: geneInfo.associated_gene_name,
                        });
                        loadAndPlotGraph();
                        return true;
                    }
                })
            },
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            return $("<li></li>").append("<a> <div class='symbol'>" + item.associated_gene_name + "</div><div class='aliases'>" + item.associated_gene_synonym + "</div><div class='description'>" + item.description + "</div><div class='clear'></div></a>").appendTo(ul);
        };
    }

    /**
     * Hides and shows controls
     */
    function toggleControlsVisibility() {
        [
            sampleGroupElement,
            clusterElement,
            geneSymbolElement,
            geneSymbolXElement,
            geneSymbolYElement,
            geneSetElement,
            geneSetXElement,
            geneSetYElement,
            plotByXElement,
            plotByYElement,
        ].forEach(function (element) {
            element.style.display = 'none';
        });
        if (plotType === GENE_VS_GENE) {
            plotByXElement.style.display = '';
            plotByYElement.style.display = '';
            plotByElement.options[2].disabled = true; // disable two options: gene and gene set
            plotByElement.options[3].disabled = true;
            if (plotBy.bySampleGroup()) {
                sampleGroupElement.style.display = '';
            }
            else {
                clusterElement.style.display = '';
            }
            if (plotByX.byGene()) {
                geneSymbolXElement.style.display = ''
            }
            else { // gene set
                geneSetXElement.style.display = ''
            }
            if (plotByY.byGene()) {
                geneSymbolYElement.style.display = ''
            }
            else { // gene set
                geneSetYElement.style.display = ''
            }
        }
        else {
            plotByElement.options[2].disabled = false;
            plotByElement.options[3].disabled = false;
            switch (plotBy.value()) {
                case SAMPLE_GROUP:
                    sampleGroupElement.style.display = '';
                    break;
                case CLUSTER:
                    clusterElement.style.display = '';
                    break;
                case GENE:
                    geneSymbolElement.style.display = '';
                    break;
                case GENE_SET:
                    geneSetElement.style.display = '';
                    break;
            }
        }
    }

    /**
     * Replots graph and changes url
     * @param fn: Function(event)
     * @returns Function
     */
    function paramChanged(fn) {
        return function (event) {
            fn(event);
            applyConfigToUrl();
            loadAndPlotGraph();
        }
    }

    /**
     * Transforms object into GET URI params
     * @param obj: {}
     * @returns string
     */
    function encodeObjectToURI(obj) {
        return '?' + Object.keys(obj)
            .map(function (key) {
                return key + '=' + encodeURIComponent(obj[key])
            })
            .join("&")
    }

    /**
     * URI GET parameters object
     * @returns {{
     * dataSetId: string,
     * plotType: string,
     * plotBy: string,
     * sampleGroup?: string,
     * cluster?: string,
     * gene?: string,
     * geneX?: string,
     * geneY?: string,
     * geneSet?: string,
     * geneSetX?: string,
     * geneSetY?: string,
     * }}
     */
    function getUrlParams() {
        const params = {
            ds_id: dataSetId,
            plotType: plotType,
            plotBy: plotBy.value(),
        };

        switch (plotBy.value()) {
            case SAMPLE_GROUP:
                params.sampleGroup = sampleGroup || '';
                break;
            case CLUSTER:
                params.cluster = cluster || '';
                break;
            case GENE:
                params.gene = gene.value || '';
                break;
            case GENE_SET:
                params.geneSet = geneSet || '';
                break;
        }
        if (plotType === GENE_VS_GENE) {
            params.plotByX = plotByX.value();
            params.plotByY = plotByY.value();
            if (plotByX.byGene() && geneX.value) {
                params.geneX = geneX.value
            }
            else if (plotByX.byGeneSet() && geneSetX) {
                params.geneSetX = geneSetX
            }
            if (plotByY.byGene() && geneY) {
                params.geneY = geneY.value
            }
            else if (plotByY.byGeneSet() && geneSetY) {
                params.geneSetY = geneSetY
            }
        }
        return params
    }

    /**
     * Changes url according current variable values
     */
    function applyConfigToUrl() {
        window.history.replaceState(document.title, document.title, location.pathname + encodeObjectToURI(getUrlParams()));
    }

    function showError(error) {
        document.getElementById('noDataForGeneError').style.display = 'block';
        document.getElementById('noDataForGeneError').innerText = error
    }

    function hideError() {
        document.getElementById('noDataForGeneError').style.display = 'none';
    }

    /**
     * ===================
     * FUNCTIONS FOR PLOTTING
     * ===================
     */
    /**
     * Clears plots
     */
    function clearGraphs() {
        document.getElementById('graph1').innerHTML = '';
        document.getElementById('graph2').innerHTML = '';
    }

    /**
     * Main function to replot graph
     */
    function loadAndPlotGraph() {
        hideError();
        const params = getMissingValuesFromCache();
        if (Object.keys(params).length > 1) { // data set id is always exists
            if (Object.keys(params).length > 2 || !params.gene) {
                // Don't try to get only gene info from iscandar controller. It doesn't have it
                getData('/iscandar/graph_data' + encodeObjectToURI(params));
            }
            if (params.gene) {
                // get gene info
                params.gene.forEach(function (gene) {
                    getGeneData(dbId, params.ds_id, gene, 'ensemblID')
                })
            }
        }
        else { // if all necessary data is already stored in cache
            applyChosenGraph();
        }
    }

    /**
     * Returns which params are needed to download
     * @returns {{
     * ds_id: string,
     * plotType: string,
     * plotBy: string,
     * sampleGroup?: string,
     * cluster?: string,
     * gene?: string[],
     * geneSet?: string,
     * }}
     */
    function getMissingValuesFromCache() {
        const params = {
            ds_id: dataSetId,
        };
        if (!cache.sampleIds) {
            params.sampleIds = true;
        }
        if (!cache.pca && (plotType === PCA_AND_TSNE || plotType === PCA)) {
            params.pca = true;
        }
        if (!cache.tsne && (plotType === PCA_AND_TSNE || plotType === TSNE)) {
            params.tsne = true;
        }
        switch (plotBy.value()) {
            case SAMPLE_GROUP:
                if (!cache.sampleGroup[sampleGroup] && sampleGroup) {
                    params.sampleGroup = sampleGroup;
                }
                break;
            case CLUSTER:
                if (!cache.cluster[cluster] && cluster) {
                    params.cluster = cluster;
                }
                break;
            case GENE:
                if (!cache.gene[gene.value] && gene.value) {
                    params.gene = [gene.value];
                }
                break;
            case GENE_SET:
                if (!cache.geneSet[geneSet] && geneSet) {
                    params.geneSet = [geneSet];
                }
                break;
        }
        if (plotType === GENE_VS_GENE) {
            if (plotByX.byGene() && geneX.value && !cache.gene[geneX.value]) {
                params.gene = [geneX.value]
            }
            else if (plotByX.byGeneSet() && geneSetX && !cache.geneSet[geneSetX]) {
                params.geneSet = [geneSetX]
            }
            if (plotByY.byGene() && geneY.value && !cache.gene[geneY.value]) {
                params.gene ? params.gene.push(geneY.value) : params.gene = [geneY.value]
            }
            else if (plotByY.byGeneSet() && geneSetY && !cache.geneSet[geneSetY]) {
                params.geneSet ? params.geneSet.push(geneSetY) : params.geneSet = [geneSetY]
            }
        }
        return params;
    }


    /**
     * Loads data from server and places data into cache
     * @param url: string
     */
    function getData(url) {
        loader.show();
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4) {
                if (this.status === 200) {
                    const response = JSON.parse(this.responseText);
                    const data = response.data;
                    const replaceKeys = [PCA, TSNE, 'sampleIds'];
                    replaceKeys.forEach(function (key) { // replace null values in cache
                        if (data[key]) {
                            cache[key] = data[key]
                        }
                    });
                    const assignKeys = [SAMPLE_GROUP, GENE, GENE_SET];
                    assignKeys.forEach(function (key) { // appends missing values to cache
                        if (data[key]) {
                            Object.assign(cache[key], data[key]);
                        }
                    });
                    applyChosenGraph();
                    if (response.error) {
                        showError(response.error);
                    }
                }
                else {
                    clearGraphs();
                    showError(this.statusText);
                }
                loader.hide();
            }
        };
        xhttp.open("GET", url, true);
        xhttp.send();
    }

    /**
     * Loads gene data from server and places data into cache
     * @param url: string
     */
    function getGeneData(db_id, ds_id, ref_id, ref_type) {
        const params = {
            db_id: db_id,
            ds_id: ds_id,
            ref_id: ref_id,
            ref_type: ref_type,
            format_type: 'iscandar',
        };
        const url = '/expressions/graph_data' + encodeObjectToURI(params);
        loader.show();
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4) {
                if (this.status === 200) {
                    const response = JSON.parse(this.responseText);
                    cache.gene[ref_id] = response.data;
                    applyChosenGraph();
                    if (response.error) {
                        showError(response.error);
                    }
                }
                else {
                    clearGraphs();
                    showError(this.statusText);
                }
                loader.hide();
            }
        };
        xhttp.open("GET", url, true);
        xhttp.send();
    }

    /**
     * Creates traces depends on configuration
     * @param plotType
     * @returns any[]
     */
    function createTracesForPCAOrTSNE(plotType) {
        hideError();
        if (plotBy.bySampleGroup() && sampleGroup || plotBy.byCluster() && cluster) {
            const groupId = plotBy.bySampleGroup() ? sampleGroup : cluster;
            if (!groupId) { // if no cluster or sample group chosen due to lack of options
                return [];
            }
            const indexesMap = {};
            let pointSize = 6;
            if (cache[plotBy.value()][groupId].length > 9000) {
                pointSize = 3;
            }
            else if (cache[plotBy.value()][groupId].length > 4000) {
                pointSize = 4;
            }
            cache[plotBy.value()][groupId].forEach(function (sampleGroupItem, index) {
                if (!indexesMap[sampleGroupItem]) {
                    indexesMap[sampleGroupItem] = {
                        x: [],
                        y: [],
                        mode: 'markers',
                        type: 'scattergl',
                        text: [],
                        name: sampleGroupItem,
                        marker: {size: pointSize, symbol: 'circle'},
                    }
                }
                indexesMap[sampleGroupItem].x.push(cache[plotType].x[index]);
                indexesMap[sampleGroupItem].y.push(cache[plotType].y[index]);
                indexesMap[sampleGroupItem].text.push(cache.sampleIds[index]);
            });
            return Object.keys(indexesMap).map(function (key) {
                return indexesMap[key];
            })
        }
        if (plotBy.byGene() && gene.value) {
            let text, color;
            if (!cache.sampleIds) { // the data has not been loaded yet
                return [];
            }
            if (cache[plotBy.value()][gene.value]) { // if gene is exists in data set
                text = cache.sampleIds.map(function (sampleId, index) {
                    return sampleId + "(" + cache[plotBy.value()][gene.value][sampleId] + ")";
                });
                color = cache.sampleIds.map(function (sampleId) {
                    return cache[plotBy.value()][gene.value][sampleId]
                });
            }
            else {
                text = cache.sampleIds; // If requested gene is not found, we display only sample ids
                color = {};
                showError('Gene not found');
            }
            return [{
                x: cache[plotType].x,
                y: cache[plotType].y,
                name: gene.name,
                type: 'scattergl',
                mode: 'markers',
                text: text,
                marker: {
                    size: 6, symbol: 'circle',
                    color: color,
                    colorbar: color ? {title: gene.name} : undefined, // hide color bar if there is no gene data
                },
            }]
        }
        if (plotBy.byGeneSet() && geneSet) {
            let text, color;
            if (cache[plotBy.value()][geneSet]) { // if gene is exists in data set
                text = cache[plotBy.value()][geneSet].map(function (value, index) {
                    return cache.sampleIds[index] + "(" + value + ")";
                });
                color = cache[plotBy.value()][geneSet];
            }
            else {
                text = cache.sampleIds; // If requested gene is not found, we display only sample ids
                color = undefined;
                showError('Gene not found');
            }
            return [{
                x: cache[plotType].x,
                y: cache[plotType].y,
                name: geneSet,
                type: 'scattergl',
                mode: 'markers',
                text: text,
                marker: {
                    size: 6, symbol: 'circle',
                    color: color,
                    colorbar: color ? {title: geneSet} : undefined, // hide color bar if there is no gene data
                },
            }]
        }
    }

    /**
     * Creates traces depends on configuration
     * @param plotType
     * @returns any[]
     */
    function createTracesForGeneVsGene() {
        const groupId = plotBy.bySampleGroup() ? sampleGroup : cluster;
        const indexesMap = {};
        const plotByKeyX = plotByX.byGene() ? geneX.value : geneSetX;
        const plotByKeyY = plotByY.byGene() ? geneY.value : geneSetY;
        console.log(cache, plotByKeyX, plotByKeyY);
        if (!plotBy.byCluster() && !plotBy.bySampleGroup() || !cache[plotByX.value()][plotByKeyX] || !cache[plotByY.value()][plotByKeyY] || !groupId) {
            // if no cluster or sample group chosen due to lack of options
            // if plot by is gene or gene set
            // if missing values in cache
            return [];
        }
        let xValues = [];
        let yValues = [];
        if (plotByX.byGene()) {
            xValues = cache.sampleIds.map(function (sampleId) {
                return cache.gene[geneX.value][sampleId]
            })
        }
        else {
            xValues = cache[plotByX.value()][plotByKeyX]
        }
        if (plotByY.byGene()) {
            yValues = cache.sampleIds.map(function (sampleId) {
                return cache.gene[geneY.value][sampleId]
            })
        }
        else {
            yValues = cache[plotByY.value()][plotByKeyY]
        }
        cache[plotBy.value()][groupId].forEach(function (sampleGroupItem, index) {
            if (!indexesMap[sampleGroupItem]) {
                indexesMap[sampleGroupItem] = {
                    x: [],
                    y: [],
                    mode: 'markers',
                    type: 'scattergl',
                    text: [],
                    name: sampleGroupItem,
                    marker: {size: 6, symbol: 'circle'},
                }
            }
            indexesMap[sampleGroupItem].x.push(xValues[index]);
            indexesMap[sampleGroupItem].y.push(yValues[index]);
            indexesMap[sampleGroupItem].text.push(cache.sampleIds[index]);
        });
        return Object.keys(indexesMap).map(function (key) {
            return indexesMap[key];
        })
    }

    /**
     * Plots graph using values from cache
     */
    function applyChosenGraph() {
        clearGraphs();
        if (plotType === GENE_VS_GENE) {
            const title = (plotByX.byGene() ? geneX.name : geneSetX) + ' vs ' + (plotByY.byGene() ? geneY.name : geneSetY);
            plotGraph(createTracesForGeneVsGene(), 'graph1', title, {
                xaxis: {title: plotByX.byGene() ? geneX.name : geneSetX},
                yaxis: {title: plotByY.byGene() ? geneY.name : geneSetY},
            });
        }
        else if (plotType !== PCA_AND_TSNE) {
            plotGraph(createTracesForPCAOrTSNE(plotType), 'graph1', plotType.toUpperCase());
        }
        else {
            plotGraph(createTracesForPCAOrTSNE(PCA), 'graph1', 'PCA');
            plotGraph(createTracesForPCAOrTSNE(TSNE), 'graph2', 'TSNE');
        }
    }

    /**
     * Real plotting data in specific object
     * @param traces: any[]
     * @param divId: string
     * @param title: string
     * @param layoutOptions: {}
     */
    function plotGraph(traces, divId, title, layoutOptions) {
        if (!layoutOptions) {
            layoutOptions = {}
        }
        Plotly.newPlot(divId, traces, Object.assign({}, layout, {title: title}, layoutOptions));
    }

    /** Here we go */
    toggleControlsVisibility();
    loadAndPlotGraph();
})();
