import json
from collections import OrderedDict


class JsonValueInterceptor(object):
    """
    This class helps you to combine json with object without parsing serialized object.
    Example of usage:
    some_json = '{"x": 9, "y": 0}
    some_another_json = '{"x": [1]}
    lazy_json = JsonValueInterceptor()
    some_object = dict()
    some_object['value'] = lazy_json.register_value(some_json)
    some_object['value2'] = lazy_json.register_value(some_another_json)
    # now some_object holds two string values: ***1*** and ***2***
    # the next sept - get json with expected values
    full_json = lazy_json.json_dump(some_object)
    """
    def __init__(self):
        self._id_value_map = {}
        self._id = 1

    def _get_id(self):
        self._id += 1
        return '***' + str(self._id) + '***'  # ***100***

    def register_value(self, value):
        id = self._get_id()
        self._id_value_map[id] = value
        return id

    def json_dump(self, obj):
        js = json.dumps(obj)
        key_position_map = {}
        # find positions in string where we need to replace our id to it's values
        for id, value in self._id_value_map.items():
            key_position_map[id] = js.find('"' + id + '"')  # "***100***" - json encoded string
        # sort positions
        key_position_map = OrderedDict(sorted(key_position_map.items(), key=lambda item: item[1], reverse=True))
        # replace ids to it's keys
        for id, position in key_position_map.items():
            js = js[:position] + self._id_value_map[id] + js[position + len(id) + 2:]
        return js
