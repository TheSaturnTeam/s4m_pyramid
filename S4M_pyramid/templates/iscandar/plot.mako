<%inherit file="../default.html"/>\
<%namespace name="Base" file="../base.mako"/>
<%def name="includes()">
</%def>
<style>
    #loading {
        position: fixed;
        z-index: 999;
        width: 100vw;
        height: 100vh;
        top: 0;
        left: 0;
        background: rgba(0, 0, 0, 0.69);
        color: white;
    }

    #loading .loading-content {
        margin: auto;
        width: 300px;
        margin-top: 33vh;
        font-size: 20px;
    }

    .container {
        width: 1006px;
        margin: auto;
    }

    .gene-input {
        padding-top: 3px;
        padding-bottom: 1px;
        font-size: inherit;
        vertical-align: text-bottom;
        border: none;
        border-bottom: 1px solid #ccc;
        color: #000;
        width: 100px;
        cursor: initial !important;
        color: #ca3b00;
    }

    .iscandar-select {
        vertical-align: text-bottom;
        border: none;
        border-bottom: 1px solid #ccc;
        color: #ca3b00;
    }

    #noDataForGeneError {
        font-size: 24px;
        display: block;
        width: 100%;
        color: red;
    }

    .controls {
        margin-top: 25px;
        font-size: larger;
        text-align: center;
    }

    .displayGraphs {
        margin-top: 20px;
    }
</style>
<div class="hidden">
    <div id="ds_id">${c.ds_id}</div>
    <div id="db_id">${c.db_id}</div>
    <input id="geneId" value="${c.gene.gene_id}"/>
    <input id="geneIdX" value="${c.geneX.gene_id}"/>
    <input id="geneIdY" value="${c.geneY.gene_id}"/>
</div>
<div class="container">
    <div class="breadcrumbs" id="breadcrumb">
        <a class="basic_link" href="${h.url('/datasets/search')}">Dataset browser</a> >>
        <a class="basic_link" id="searchGene" href="${h.url('/datasets/search?ds_id=')}${c.ds_id}">Summary</a> >> <span
            class="current">Graph</span>
    </div>
</div>
<div class="clear"></div>
<div class="container">
    <div class="title">${c.dataset[c.ds_id]['title']}</div>
    <div class="handle">${c.dataset[c.ds_id]['handle']} (${c.dataset[c.ds_id]['organism']})</div>
    <div class="cells">${c.dataset[c.ds_id]['cells_samples_assayed']}</div>
    <div class="clear"></div>
    <div class="description">${c.dataset[c.ds_id]['description']}</div>
    <div class="controls">
        <%def name="geneSetOptions(val)">
            % for g in c.model_info['genesets']:
                <option value="${g}" ${('selected' if g == val else '')}>${g}</option>
            % endfor
        </%def>
        Plot <select id="plotType" class="iscandar-select">
        <%
            names = {'pca': 'pca', 'tsne': 'tsne', 'pca_tsne': 'pca & tsne', 'gene_vs_gene': 'gene vs gene'}
        %>
        % for g in ['pca', 'tsne', 'pca_tsne', 'gene_vs_gene']:
            <option value="${g}" ${('selected' if g == c.plotType else '')}>${names[g]}</option>
        % endfor
    </select>
        by <select id="plotBy" class="iscandar-select">
        <%
            names = {
            'sampleGroup': 'sample group',
            'cluster': 'cluster',
            'gene': 'gene',
            'geneSet': 'gene set',
            }
        %>
        % for plot_by_option in ['sampleGroup', 'cluster', 'gene', 'geneSet']:
            <option value="${plot_by_option}" ${('selected' if plot_by_option == c.plotBy else '')}>${names[plot_by_option]}</option>
        % endfor
    </select>
        <select id="sampleGroup" class="iscandar-select">
            % for sample_group_option in c.model_info['sample_groups']:
                <option value="${sample_group_option}" ${('selected' if sample_group_option == c.sample_group else '')}>${sample_group_option}</option>
            % endfor
        </select>
        <select id="cluster" class="iscandar-select">
            % for cluster in c.model_info['clusters']:
                <option value="${cluster}" ${('selected' if cluster == c.cluster else '')}>${cluster}</option>
            % endfor
        </select>
        <select id="geneSet" class="iscandar-select">
            ${geneSetOptions(c.geneSet)}
        </select>
        <select id="plotByX" class="iscandar-select">
            <%
                names = {
            'gene': 'gene',
            'geneSet': 'gene set',
            }
            %>
            % for plot_by_option in [ 'gene', 'geneSet']:
                <option value="${plot_by_option}" ${('selected' if plot_by_option == c.plotByX else '')}>${names[plot_by_option]}</option>
            % endfor
        </select>
        <input id="gene" class="gene-input" placeholder="gene" value="${c.gene.associated_gene_name}"/>
        <input id="geneX" class="gene-input" placeholder="gene X" value="${c.geneX.associated_gene_name}"/>
        <select id="geneSetX" class="iscandar-select">
            ${geneSetOptions(c.geneSetX)}
        </select>
        <select id="plotByY" class="iscandar-select">
            % for plot_by_option in [ 'gene', 'geneSet']:
                <option value="${plot_by_option}" ${('selected' if plot_by_option == c.plotByY else '')}>${names[plot_by_option]}</option>
            % endfor
        </select>
        <input id="geneY" class="gene-input" placeholder="gene Y" value="${c.geneY.associated_gene_name}"/>
        <select id="geneSetY" class="iscandar-select">
            ${geneSetOptions(c.geneSetY)}
        </select>
    </div>
</div>

<h1 id="noDataForGeneError" style="display: none;">
</h1>
<div id="notification" class="notification">
    <div class="notification-title">
    </div>
</div>
<div class="displayGraphs">
    <div id="graph1" style="width: 100%; height: 500px">
    </div>
    <div id="graph2" style="width: 100%">
    </div>
</div>

<div id="loading">
    <div class="loading-content">

        <div class="message">
            Loading...
        </div>
        <img src="/images/loading.svg"/>
    </div>
</div>
<script type="text/javascript" src="/js/iscandar/plotly-latest.min.js"></script>
<script type="text/javascript" src="/js/iscandar/plot_graph.js"></script>