#!/bin/bash

psql -U portaladmin portal_prod -c "update stemformatics.configs set ref_id = 'v6.8.2' where ref_type = 'stemformatics_version';"
psql -U portaladmin portal_prod -c "update stemformatics.configs set ref_id = 'true' where ref_type = 'use_cdn';"
psql -U portaladmin portal_prod -c "update stemformatics.configs set ref_id = 'stemformatics.sa.metacdn.com/release_682' where ref_type = 'cdn_base_url';"

wget http://localhost:5000/api/trigger_config_update -O /dev/null

psql -U portaladmin portal_prod -c "delete from dataset_metadata where ds_name = 'showReportOnDatasetSummaryPage' and ds_value ilike '%PCA%';"

echo "Now you will need to upgrade the help to v6.8.2"
