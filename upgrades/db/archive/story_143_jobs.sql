
-- Table: stemformatics.jobs

CREATE TABLE stemformatics.jobs
(
  job_id serial NOT NULL,
  analysis integer NOT NULL,
  status integer NOT NULL,
  dataset_id integer NOT NULL,
  gene_set_id integer NOT NULL,
  uid integer NOT NULL,
  created timestamp without time zone DEFAULT now(),
  use_cls boolean,
  use_gct boolean,
  reference_type text,
  reference_id text,
  CONSTRAINT job_id PRIMARY KEY (job_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE stemformatics.jobs OWNER TO ascc_dev;

