-- "First run" (wipe-clean) dataset gene <-> sample detection stats update SQL.
-- NOTE: DO NOT RUN DIRECTLY.
-- This SQL script contains %DATASET_ID% variable that must be file-replaced
-- prior to execution.
-- The wrapper script that does this automagically is:  update_stats_for_datasets.sh
-- For usage instructions, run ./update_stats_for_datasets.sh (without args)
--
-- NOTE: This script generates false-positive errors RE: existing tables.
--       This behaviour is expected and can be safely ignored.
--
-- OK 2011-07-21

DELETE FROM stemformatics.dataset_chip_type_map;

INSERT INTO stemformatics.dataset_chip_type_map
SELECT md.ds_id, ap.chip_type
FROM public.assay_platforms ap
INNER JOIN
(SELECT
 m.ds_id, m.ds_value AS "manufacturer", p.ds_value AS "platform", v.ds_value AS "version"
FROM
 public.dataset_metadata m
 INNER JOIN (SELECT * FROM public.dataset_metadata WHERE ds_name = 'Assay Platform') p ON m.ds_id = p.ds_id
 INNER JOIN (SELECT * FROM public.dataset_metadata WHERE ds_name = 'Assay Version') v ON m.ds_id = v.ds_id
WHERE m.ds_name = 'Assay Manufacturer' AND m.ds_id=%DATASET_ID%) md ON md.manufacturer = ap.manufacturer AND md.platform = ap.platform AND md.version = ap.version;

CREATE TABLE TEMP_chip_probe (
   chip_type integer NOT NULL DEFAULT 0,
   probe_id text NOT NULL,
   CONSTRAINT TEMP_chip_probe_pkey PRIMARY KEY (chip_type, probe_id)
);
DELETE FROM TEMP_chip_probe;

INSERT INTO TEMP_chip_probe
(SELECT distinct pe.chip_type, pe.probe_id FROM public.probe_expressions pe WHERE pe.ds_id=%DATASET_ID%);

DELETE FROM stemformatics.stats_detectedprobes_count;

INSERT INTO stemformatics.stats_detectedprobes_count
SELECT dcm.ds_id,  cp.chip_type, cp.probe_id, count(ard.replicate_group_id) AS detected
FROM stemformatics.dataset_chip_type_map dcm
INNER JOIN TEMP_chip_probe cp ON cp.chip_type = dcm.chip_type
LEFT JOIN ( SELECT ar.ds_id, ar.chip_type, ar.probe_id, ar.replicate_group_id
       FROM stemformatics.probe_expressions_avg_replicates ar
       WHERE ar.detected = true) ard
   ON ard.chip_type = cp.chip_type AND ard.probe_id = cp.probe_id
GROUP BY dcm.ds_id, cp.chip_type, cp.probe_id
HAVING dcm.ds_id=%DATASET_ID%;


-- 
-- stemformatics.stats_datasetsample_count
--
DELETE FROM stemformatics.stats_datasetsample_count;

INSERT INTO stemformatics.stats_datasetsample_count
SELECT bs.ds_id, bs.chip_type, count(distinct bsm.md_value)  AS samples
FROM public.biosamples bs
JOIN public.biosamples_metadata bsm ON bsm.chip_id = bs.chip_id
WHERE bsm.md_name = 'Replicate Group ID'
GROUP BY bs.ds_id, bs.chip_type
HAVING bs.ds_id=%DATASET_ID%;

-- TEMP_sample_probe_count

-- DEBUG ONLY
-- \set VERBOSITY verbose

CREATE TABLE TEMP_sample_probe_count (
   database_id integer NOT NULL,
   gene_id text NOT NULL,
   dataset_id integer NOT NULL,
   chip_type integer NOT NULL DEFAULT 0,
   replicate_group_id text NOT NULL,
   probe_count integer NOT NULL DEFAULT 0,
   CONSTRAINT TEMP_sample_probe_count_pkey PRIMARY KEY (database_id, gene_id, dataset_id, chip_type, replicate_group_id)
);
DELETE FROM TEMP_sample_probe_count;

INSERT INTO TEMP_sample_probe_count
SELECT pm.database_id, pm.identifier, ard.ds_id, pm.chip_type, ard.replicate_group_id, count(ard.probe_id) AS probe_count
FROM public.probe_mappings pm
INNER JOIN stemformatics.probe_expressions_avg_replicates ard ON ard.chip_type = pm.chip_type AND ard.probe_id = pm.probe_id
GROUP BY pm.database_id, pm.identifier, ard.ds_id, pm.chip_type, ard.replicate_group_id
HAVING ard.ds_id=%DATASET_ID%
;

-- DEBUG ONLY
-- \set VERBOSITY verbose

-- TEMP_sample_detected_probe_count

CREATE TABLE TEMP_sample_detected_probe_count (
   database_id integer NOT NULL,
   gene_id text NOT NULL,
   dataset_id integer NOT NULL,
   chip_type integer NOT NULL DEFAULT 0,
   replicate_group_id text NOT NULL,
   detected_probe_count integer NOT NULL DEFAULT 0,
   CONSTRAINT TEMP_sample_detected_probe_count_pkey PRIMARY KEY (database_id, gene_id, dataset_id, chip_type, replicate_group_id)
);
DELETE FROM TEMP_sample_detected_probe_count;

INSERT INTO TEMP_sample_detected_probe_count
SELECT pm.database_id, pm.identifier, ard.ds_id, pm.chip_type,  ard.replicate_group_id, count(ard.probe_id) AS detected_probe_count
FROM public.probe_mappings pm
INNER JOIN stemformatics.probe_expressions_avg_replicates ard ON ard.chip_type = pm.chip_type AND ard.probe_id = pm.probe_id AND ard.detected = true
GROUP BY pm.database_id, pm.identifier, ard.ds_id, pm.chip_type, ard.replicate_group_id
HAVING ard.ds_id=%DATASET_ID%
;
-- TEMP_sample_count

CREATE TABLE TEMP_sample_count (
   database_id integer NOT NULL,
   gene_id text NOT NULL,
   sample_count integer NOT NULL DEFAULT 0,
   CONSTRAINT TEMP_sample_count_pkey PRIMARY KEY (database_id, gene_id)
);
-- OK: Keep this
DELETE FROM TEMP_sample_count;

-- OK: Not modified from original. Runtime should not be dependant upon number of datasets loaded.

INSERT INTO TEMP_sample_count
SELECT  ga.db_id AS database_id, ga.gene_id, count(distinct sc.replicate_group_id) AS sample_count
FROM public.genome_annotations ga
LEFT JOIN TEMP_sample_probe_count sc ON sc.database_id = ga.db_id AND sc.gene_id = ga.gene_id
GROUP BY ga.db_id, ga.gene_id;


-- TEMP_sample_detected_count

CREATE TABLE TEMP_sample_detected_count (
   database_id integer NOT NULL,
   gene_id text NOT NULL,
   detected_sample_count integer NOT NULL DEFAULT 0,
   CONSTRAINT TEMP_sample_detected_count_pkey PRIMARY KEY (database_id, gene_id)
);
-- OK: Keep this
DELETE FROM TEMP_sample_detected_count;

-- OK: Not modified, runtime not affected by number of datasets loaded.

INSERT INTO TEMP_sample_detected_count
SELECT  ga.db_id AS database_id, ga.gene_id, count(distinct sc.replicate_group_id) AS detected_count
FROM public.genome_annotations ga
LEFT JOIN TEMP_sample_detected_probe_count sc ON sc.database_id = ga.db_id AND sc.gene_id = ga.gene_id
GROUP BY ga.db_id, ga.gene_id;

--
-- stemformatics.stats_genedetected
--

-- OK: Keep this
DELETE FROM stemformatics.stats_genedetected;

-- OK: Not modified.

INSERT INTO stemformatics.stats_genedetected
SELECT sc.database_id, sc.gene_id, scd.detected_sample_count, sc.sample_count
FROM   TEMP_sample_count sc
LEFT JOIN TEMP_sample_detected_count scd ON scd.database_id = sc.database_id AND scd.gene_id = sc.gene_id;

-- 
-- stemformatics.stats_datasetsummary
--

-- OK: Keep this
DELETE FROM stemformatics.stats_datasetsummary;

-- OK: Not modified. Independent of datasets loaded, very fast.

INSERT INTO stemformatics.stats_datasetsummary
SELECT b.ds_id, bmd.md_name, bmd.md_value, count(distinct bmd.chip_id)
FROM biosamples b
INNER JOIN biosamples_metadata bmd ON b.chip_id = bmd.chip_id
LEFT JOIN biosamples_metadata rgi ON rgi.chip_id = bmd.chip_id AND rgi.md_name = 'Replicate Group ID'
WHERE bmd.chip_id in
       (SELECT min(chip_id)
       FROM biosamples_metadata
       WHERE md_name = 'Replicate Group ID'
       GROUP BY md_value)
AND bmd.md_name in ('Gender', 'Cell Line', 'Cell Type', 'Developmental Stage', 'Disease State', 'FACS Profile', 'Labelling', 'Organism', 'Organism Part', 'Pregnancy Status', 'Sample Type', 'Tissue')
GROUP BY b.ds_id, bmd.md_name, bmd.md_value
ORDER BY b.ds_id, bmd.md_name, bmd.md_value;

