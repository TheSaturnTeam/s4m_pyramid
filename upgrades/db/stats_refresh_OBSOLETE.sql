--
-- stemformatics.dataset_chip_type_map
--
-- NOTE: Need to run this after every dataset is loaded / or after a bulk load.
--
DELETE FROM stemformatics.dataset_chip_type_map;
INSERT INTO stemformatics.dataset_chip_type_map
SELECT md.ds_id, ap.chip_type
FROM public.assay_platforms ap
INNER JOIN
(SELECT
 m.ds_id, m.ds_value AS "manufacturer", p.ds_value AS "platform", v.ds_value AS "version"
FROM
 public.dataset_metadata m
 INNER JOIN (SELECT * FROM public.dataset_metadata WHERE ds_name = 'Assay Platform') p ON m.ds_id = p.ds_id
 INNER JOIN (SELECT * FROM public.dataset_metadata WHERE ds_name = 'Assay Version') v ON m.ds_id = v.ds_id
WHERE m.ds_name = 'Assay Manufacturer') md ON md.manufacturer = ap.manufacturer AND md.platform = ap.platform AND md.version = ap.version;


-- Used in stemformatics.stats_detectedprobes_count query (next)
DROP TABLE IF EXISTS TEMP_chip_probe;
CREATE TEMPORARY TABLE TEMP_chip_probe (
   chip_type integer NOT NULL DEFAULT 0,
   probe_id text NOT NULL,
   CONSTRAINT TEMP_chip_probe_pkey PRIMARY KEY (chip_type, probe_id)
);

DELETE FROM TEMP_chip_probe;
INSERT INTO TEMP_chip_probe
SELECT distinct pe.chip_type, pe.probe_id FROM public.probe_expressions pe
INNER JOIN stemformatics.dataset_chip_type_map dcm ON dcm.ds_id = pe.ds_id;

-- 
-- stemformatics.stats_detectedprobes_count
--
DELETE FROM stemformatics.stats_detectedprobes_count;
INSERT INTO stemformatics.stats_detectedprobes_count
SELECT dcm.ds_id,  cp.chip_type, cp.probe_id, count(ard.replicate_group_id) AS detected
FROM stemformatics.dataset_chip_type_map dcm
INNER JOIN TEMP_chip_probe cp ON cp.chip_type = dcm.chip_type
LEFT JOIN ( SELECT ar.ds_id, ar.chip_type, ar.probe_id, ar.replicate_group_id
       FROM stemformatics.probe_expressions_avg_replicates ar
       WHERE ar.detected = true ) ard
   ON ard.chip_type = cp.chip_type AND ard.probe_id = cp.probe_id
GROUP BY dcm.ds_id, cp.chip_type, cp.probe_id;


-- 
-- stemformatics.stats_datasetsample_count
--
DELETE FROM stemformatics.stats_datasetsample_count;

INSERT INTO stemformatics.stats_datasetsample_count
SELECT bs.ds_id, bs.chip_type, count(distinct bsm.md_value)  AS samples
FROM public.biosamples bs
JOIN public.biosamples_metadata bsm ON bsm.chip_id = bs.chip_id
WHERE bsm.md_name = 'Replicate Group ID'
GROUP BY bs.ds_id, bs.chip_type;

-- TEMP_sample_probe_count
DROP TABLE IF EXISTS TEMP_sample_probe_count;

CREATE TEMPORARY TABLE TEMP_sample_probe_count (
   database_id integer NOT NULL,
   gene_id text NOT NULL,
   dataset_id integer NOT NULL,
   chip_type integer NOT NULL DEFAULT 0,
   replicate_group_id text NOT NULL,
   probe_count integer NOT NULL DEFAULT 0,
   CONSTRAINT TEMP_sample_probe_count_pkey PRIMARY KEY (database_id, gene_id, dataset_id, chip_type, replicate_group_id)
);

INSERT INTO TEMP_sample_probe_count
SELECT pm.database_id, pm.identifier, ard.ds_id, pm.chip_type, ard.replicate_group_id, count(ard.probe_id) AS probe_count
FROM public.probe_mappings pm
INNER JOIN stemformatics.probe_expressions_avg_replicates ard ON ard.chip_type = pm.chip_type AND ard.probe_id = pm.probe_id
GROUP BY pm.database_id, pm.identifier, ard.ds_id, pm.chip_type, ard.replicate_group_id;


-- TEMP_sample_detected_probe_count
DROP TABLE IF EXISTS TEMP_sample_detected_probe_count;

CREATE TEMPORARY TABLE TEMP_sample_detected_probe_count (
   database_id integer NOT NULL,
   gene_id text NOT NULL,
   dataset_id integer NOT NULL,
   chip_type integer NOT NULL DEFAULT 0,
   replicate_group_id text NOT NULL,
   detected_probe_count integer NOT NULL DEFAULT 0,
   CONSTRAINT TEMP_sample_detected_probe_count_pkey PRIMARY KEY (database_id, gene_id, dataset_id, chip_type, replicate_group_id)
);

INSERT INTO TEMP_sample_detected_probe_count
SELECT pm.database_id, pm.identifier, ard.ds_id, pm.chip_type,  ard.replicate_group_id, count(ard.probe_id) AS detected_probe_count
FROM public.probe_mappings pm
INNER JOIN stemformatics.probe_expressions_avg_replicates ard ON ard.chip_type = pm.chip_type AND ard.probe_id = pm.probe_id AND ard.detected = true
GROUP BY pm.database_id, pm.identifier, ard.ds_id, pm.chip_type, ard.replicate_group_id;

-- TEMP_sample_count
DROP TABLE IF EXISTS TEMP_sample_count;

CREATE TEMPORARY TABLE TEMP_sample_count (
   database_id integer NOT NULL,
   gene_id text NOT NULL,
   sample_count integer NOT NULL DEFAULT 0,
   CONSTRAINT TEMP_sample_count_pkey PRIMARY KEY (database_id, gene_id)
);

INSERT INTO TEMP_sample_count
SELECT  ga.db_id AS database_id, ga.gene_id, count(distinct sc.replicate_group_id) AS sample_count
FROM public.genome_annotations ga
LEFT JOIN TEMP_sample_probe_count sc ON sc.database_id = ga.db_id AND sc.gene_id = ga.gene_id
GROUP BY ga.db_id, ga.gene_id;

-- TEMP_sample_detected_count
DROP TABLE IF EXISTS TEMP_sample_detected_count;

CREATE TEMPORARY TABLE TEMP_sample_detected_count (
   database_id integer NOT NULL,
   gene_id text NOT NULL,
   detected_sample_count integer NOT NULL DEFAULT 0,
   CONSTRAINT TEMP_sample_detected_count_pkey PRIMARY KEY (database_id, gene_id)
);

INSERT INTO TEMP_sample_detected_count
SELECT  ga.db_id AS database_id, ga.gene_id, count(distinct sc.replicate_group_id) AS detected_count
FROM public.genome_annotations ga
LEFT JOIN TEMP_sample_detected_probe_count sc ON sc.database_id = ga.db_id AND sc.gene_id = ga.gene_id
GROUP BY ga.db_id, ga.gene_id;

--
-- stemformatics.stats_genedetected
--
DELETE FROM stemformatics.stats_genedetected;

INSERT INTO stemformatics.stats_genedetected
SELECT sc.database_id, sc.gene_id, scd.detected_sample_count, sc.sample_count
FROM   TEMP_sample_count sc
LEFT JOIN TEMP_sample_detected_count scd ON scd.database_id = sc.database_id AND scd.gene_id = sc.gene_id;

-- 
-- VIEW stemformatics.stats_datasetsumary
-- DEPRECATED. REMOVE ASAP.
--
--DROP VIEW IF EXISTS stemformatics.stats_datasetsumary;
--CREATE VIEW stemformatics.stats_datasetsumary AS
--SELECT b.ds_id, bmd.md_name, bmd.md_value, count(distinct bmd.chip_id)
--FROM biosamples b
--INNER JOIN biosamples_metadata bmd ON b.chip_id = bmd.chip_id
--LEFT JOIN biosamples_metadata rgi ON rgi.chip_id = bmd.chip_id AND rgi.md_name = 'Replicate Group ID'
--WHERE bmd.chip_id in
--       (SELECT min(chip_id)
--       FROM biosamples_metadata
--       WHERE md_name = 'Replicate Group ID'
--       GROUP BY md_value)
--AND bmd.md_name in ('Gender', 'Cell Type', 'Disease State', 'Organism', 'Material Type', 'Organism Part', 'Label', 'Cell Subpopulation', 'PregnancyStatus')
--GROUP BY b.ds_id, bmd.md_name, bmd.md_value
--ORDER BY b.ds_id, bmd.md_name, bmd.md_value;

-- 
-- stemformatics.stats_datasetsummary
--
DELETE FROM stemformatics.stats_datasetsummary;

INSERT INTO stemformatics.stats_datasetsummary
SELECT b.ds_id, bmd.md_name, bmd.md_value, count(distinct bmd.chip_id)
FROM biosamples b
INNER JOIN biosamples_metadata bmd ON b.chip_id = bmd.chip_id
LEFT JOIN biosamples_metadata rgi ON rgi.chip_id = bmd.chip_id AND rgi.md_name = 'Replicate Group ID'
WHERE bmd.chip_id in
       (SELECT min(chip_id)
       FROM biosamples_metadata
       WHERE md_name = 'Replicate Group ID'
       GROUP BY md_value)
AND bmd.md_name in ('Gender', 'Cell Line', 'Cell Type', 'Developmental Stage', 'Disease State', 'Label', 'Organism', 'Organism Part', 'Pregnancy Status')
GROUP BY b.ds_id, bmd.md_name, bmd.md_value
ORDER BY b.ds_id, bmd.md_name, bmd.md_value;

