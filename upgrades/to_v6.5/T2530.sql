update dataset_metadata set ds_value = 'leukomics' where ds_name = 'project' and ds_id in (6328,6329,6324,6349,6610,6322,6326,5003,7023,6180,6611,6321,6325,7028,7026,7027,5041,1000,6213); 

insert into dataset_metadata (ds_id,ds_name,ds_value) values(6328,'subproject','leukomics:clinical parameters');
insert into dataset_metadata (ds_id,ds_name,ds_value) values(6329,'subproject','leukomics:clinical parameters');
 
insert into dataset_metadata (ds_id,ds_name,ds_value) values(6324,'subproject','leukomics:CML and normal haematopoietic cells ,leukomics:disease stage');
insert into dataset_metadata (ds_id,ds_name,ds_value) values(6349,'subproject','leukomics:CML and normal haematopoietic cells'); 
insert into dataset_metadata (ds_id,ds_name,ds_value) values(6610,'subproject','leukomics:CML and normal haematopoietic cells'); 
insert into dataset_metadata (ds_id,ds_name,ds_value) values(6322,'subproject','leukomics:CML and normal haematopoietic cells'); 
insert into dataset_metadata (ds_id,ds_name,ds_value) values(6326,'subproject','leukomics:CML and normal haematopoietic cells ,leukomics:disease stage');
insert into dataset_metadata (ds_id,ds_name,ds_value) values(5003,'subproject','leukomics:CML and normal haematopoietic cells'); 
insert into dataset_metadata (ds_id,ds_name,ds_value) values(7023,'subproject','leukomics:disease stage');
insert into dataset_metadata (ds_id,ds_name,ds_value) values(6180,'subproject','leukomics:drug treatments');
insert into dataset_metadata (ds_id,ds_name,ds_value) values(6611,'subproject','leukomics:drug treatments');
insert into dataset_metadata (ds_id,ds_name,ds_value) values(6321,'subproject','leukomics:drug treatments');
insert into dataset_metadata (ds_id,ds_name,ds_value) values(6325,'subproject','leukomics:drug treatments, leukomics:other');
insert into dataset_metadata (ds_id,ds_name,ds_value) values(7028,'subproject','leukomics:drug treatments, leukomics:mouse models');
insert into dataset_metadata (ds_id,ds_name,ds_value) values(7026,'subproject','leukomics:mouse models');
insert into dataset_metadata (ds_id,ds_name,ds_value) values(7027,'subproject','leukomics:mouse models');
insert into dataset_metadata (ds_id,ds_name,ds_value) values(5041,'subproject','leukomics:other');
insert into dataset_metadata (ds_id,ds_name,ds_value) values(1000,'subproject','leukomics:other');
insert into dataset_metadata (ds_id,ds_name,ds_value) values(6213,'subproject','leukomics:other');
