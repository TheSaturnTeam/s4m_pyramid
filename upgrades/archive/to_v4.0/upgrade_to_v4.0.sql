update datasets set handle = replace(handle, '_PRIVATE', '') where private;
ALTER TABLE datasets ADD COLUMN show_limited boolean NOT NULL default False;
ALTER TABLE stemformatics.gene_sets ADD COLUMN needs_attention boolean NOT NULL default False;

Drop table stemformatics.private_gene_sets_update_archive;
Drop type ENS_ID_STATUS;
Drop sequence id_auto_increment;

CREATE TYPE ENS_ID_STATUS AS ENUM ('retired', 'remapped');

CREATE SEQUENCE id_auto_increment;

CREATE TABLE stemformatics.private_gene_sets_update_archive (
  id integer not null default nextval('id_auto_increment'),
  gene_set_id integer not null,
  gene_set_name text not null,
  user_id integer not null,
  user_name text not null,
  species_db_id integer not null,
  old_ens_id text not null,
  new_ens_id text default '',
  ENS_ID_STATUS ens_id_status default null,
  old_ens_version integer not null,
  new_ens_version integer not null,
  old_ogs text not null,
  new_ogs text default null,
  old_entrez_id integer default null,
  new_entrez_id integer default null,
  s4m_update_version float not null,
  PRIMARY KEY(id),
  UNIQUE (gene_set_id, user_id, old_ens_id, s4m_update_version)
);
