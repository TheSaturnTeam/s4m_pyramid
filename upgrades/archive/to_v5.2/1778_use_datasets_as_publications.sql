-- clear up everything
delete from dataset_metadata where ds_name in ('Citation','Publication Date','showAsPublication');

-- setup all the fields
insert into dataset_metadata (ds_id, ds_name,ds_value) select ds_id,'showAsPublication','False' from dataset_metadata where ds_name = 'Description';
insert into dataset_metadata (ds_id, ds_name,ds_value) select ds_id,'Publication Date','' from dataset_metadata where ds_name = 'Description';

-- setup for stemformatics - matigian dataset
update dataset_metadata set ds_value = 'True' where ds_id = 2000 and ds_name = 'showAsPublication';
update dataset_metadata set ds_value = 'Dis Model Mech. 2010 Nov-Dec;3(11-12):785-98. doi: 10.1242/dmm.005447. Epub 2010 Aug 10.' where ds_id = 2000 and ds_name = 'Publication Citation';
update dataset_metadata set ds_value = '2010-08' where ds_id = 2000 and ds_name = 'Publication Date';


-- setup for stemformatics - Laslett dataset
update dataset_metadata set ds_value = 'True' where ds_id = 5008 and ds_name = 'showAsPublication';
update dataset_metadata set ds_value = 'Stem Cells. 2013 Aug;31(8):1498-510. doi: 10.1002/stem.1425.' where ds_id = 5008 and ds_name = 'Publication Citation';
update dataset_metadata set ds_value = '2013-08' where ds_id = 5008 and ds_name = 'Publication Date';

-- setup for PG 
update dataset_metadata set ds_value = 'True' where ds_id = 6131 and ds_name = 'showAsPublication';
update dataset_metadata set ds_value = 'Stem Cells. 2013 Aug;31(8):1498-510. doi: 10.1002/stem.1425.' where ds_id = 6131 and ds_name = 'Publication Citation';
update dataset_metadata set ds_value = '2013-08' where ds_id = 6131 and ds_name = 'Publication Date';


