create table stemformatics.audit_log (id integer, ref_type text, ref_id text, uid integer, date_created timestamp default now(), controller text, action text, ip_address text);

create unique index audit_log_id on stemformatics.audit_log (id);

create index audit_log_uid on stemformatics.audit_log (uid);
create index audit_log_ref_type_ref_id on stemformatics.audit_log (ref_type,ref_id);
create index audit_log_controller_action on stemformatics.audit_log (controller,action);
create index audit_log_date_created on stemformatics.audit_log (date_created);
create index audit_log_ip on stemformatics.audit_log (ip_address);


CREATE SEQUENCE stemformatics.audit_log_id_seq;
ALTER TABLE stemformatics.audit_log alter column id set default nextval('stemformatics.audit_log_id_seq');
ALTER SEQUENCE stemformatics.audit_log_id_seq OWNED BY stemformatics.audit_log.id;
