#!/bin/bash

# Have to setup Yugene on production
# Create a directory DONE
# Copy over the files DONE
# check the configuration files are turned on DONE
# setup redis server - but check memory first
/data/repo/git-working/stemformatics_tools/redis/s4m/x_platform/initialise.sh localhost /var/www/pylons-data/prod/CUMULATIVEFiles/ 0

# -----------------------------    T#725  Setup probes per dataset -------------------------

#Make a directory
gct_dataset_dir=/var/www/pylons-data/SHARED/GCTFiles
probes_per_dataset_dir=/var/www/pylons-data/SHARED/ProbeFiles

if [ ! -d "$probes_per_dataset_dir" ]; then
    mkdir -p $probes_per_dataset_dir 
    ln -s $probes_per_dataset_dir /var/www/pylons-data/prod/ProbeFiles
    ln -s $probes_per_dataset_dir /var/www/pylons-data/dev/ProbeFiles
fi 

for file in `ls $gct_dataset_dir/*.gct`; do
    ds_id=`echo $file | egrep -o [0-9]+`
    echo $ds_id
    sed '1,3d' $file | cut -f 1 > $probes_per_dataset_dir/$ds_id.probes
    
done


# -----------------------------    T#569  Line Graph -------------------------
# All this needs is the annotations to be updated for PG datasets
# 5037,6073 are in sql upgrade
# 6082, 6084,6089, 6111 have to use the reload funciton in s4m.sh


# --- T#717 Change detected values
cd /var/www/pylons-data/SHARED/GCTFiles 
chmod 744 717_change_not_detected_values.sh
./717_change_not_detected_values.sh
