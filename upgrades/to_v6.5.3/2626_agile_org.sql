-- agile_org platforms to be collapsed

-- this was the SOLiD 5500xl
-- Note: SOLiD 500xl was actually 5500xl
update datasets set platform_id = 139
where platform_id in (100,101,129,123,124,138);
 
update platforms set name = 'SOLiD 5500xl' version = '' where id = 139;

delete from platforms where id in (100,101,129,123,124,138);


 
-- this was the HiSeq 2500 
update datasets set platform_id = 231
where platform_id in (125,126,135,210,212,216,229);
 
update platforms set name = 'Illumina HiSeq 2500 (GPL16791)' version = '' where id = 231;
 
delete from platforms where id in (125,126,135,210,212,216,229);
 
 
-- this was the HiSeq 2000 
update datasets set platform_id = 218 where platform_id in (108,107,137,213,173,106,215,217,141);
 
update platforms set name = 'Illumina HiSeq 2000 (GPL13112)', version = '' where id = 218;
 
delete from platforms where id in (108,107,137,213,173,106,215,217,141);


-- Illumina NextSeq 500
update datasets set platform_id = 209
where platform_id in (208,232);
 
update platforms set name = 'Illumina NextSeq 500', version = '' where id = 209;
 
delete from platforms where id in (208,232);

-- this is for Genome Analyzer
-- D#6155 is Genome Analyzer II (113) and D#6981 is IIx (220)
 
update datasets set platform_id = 113
where platform_id in (111,112);

update platforms set name = 'Genome Analyzer II' version = '' where id = 113;
 
 
delete from platforms where id in (111,112);
update platforms set platform = 'Genome Analyzer II' where id = 113;


-- this is for Helicos HeliScope (206 wasn't available in assay_platfoms)
update datasets set platform_id = 115
where platform_id in (102,206);
 
update platforms set name = 'Helicos Biosciences HeliScope', version = '' where id = 115;
 
delete from platforms where id in (102,206);






