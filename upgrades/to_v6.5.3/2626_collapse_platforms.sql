-- assay_platforms to be collapsed

-- this was the SOLiD 5500xl
-- Note: SOLiD 500xl was actually 5500xl
update datasets set chip_type = 139
where chip_type in (100,101,129,123,124,138);
 
update biosamples_metadata set chip_type = 139
where chip_type in (100,101,129,123,124,138);
 
delete from assay_platforms where chip_type in (100,101,129,123,124,138);


 
-- this was the HiSeq 2500 
update datasets set chip_type = 231
where chip_type in (125,126,135,210,212,216,229);
 
update biosamples_metadata set chip_type = 231
where chip_type in (125,126,135,210,212,216,229);
 
delete from assay_platforms where chip_type in (125,126,135,210,212,216,229);
 
 
-- this was the HiSeq 2000 
update datasets set chip_type = 218 where chip_type in (108,107,137,213,173,106,215,217,141);
 
update biosamples_metadata set chip_type = 218 where chip_type in (108,107,137,213,173,106,215,217,141);
 
delete from assay_platforms where chip_type in (108,107,137,213,173,106,215,217,141);


-- Illumina NextSeq 500
update datasets set chip_type = 209
where chip_type in (208,232);
 
update biosamples_metadata set chip_type = 209
where chip_type in (208,232);
 
delete from assay_platforms where chip_type in (208,232);

-- this is for Genome Analyzer
-- D#6155 is Genome Analyzer II (113) and D#6981 is IIx (220)
 
update datasets set chip_type = 113
where chip_type in (111,112);
 
update biosamples_metadata set chip_type = 113
where chip_type in (111,112);
 
delete from assay_platforms where chip_type in (111,112);
update assay_platforms set platform = 'Genome Analyzer II' where chip_type = 113;


-- this is for Helicos HeliScope (206 wasn't available in assay_platfoms)
update datasets set chip_type = 115
where chip_type in (102,206);
 
update biosamples_metadata set chip_type = 115
where chip_type in (102,206);
 
delete from assay_platforms where chip_type in (102,206);




update assay_platforms set version = '',platform_type = '' where chip_type  in (139,231,218,209,113,115);

-- truncate biosamples - not in use
truncate biosamples;
