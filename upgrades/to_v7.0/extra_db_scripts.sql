
INSERT INTO stemformatics.configs (ref_type,ref_id) values('debug','false');
INSERT INTO stemformatics.configs (ref_type,ref_id) values('gene_mapping_raw_file_base_name','/var/www/pylons-data/prod/gene_mapping.raw');
INSERT INTO stemformatics.configs (ref_type,ref_id) values('feature_mapping_raw_file_base_name','/var/www/pylons-data/prod/feature_mapping.raw');
Insert into stemformatics.configs (ref_type,ref_id) values('secret_hash_parameter_for_unsubscribe', 'I LOVE WY');
INSERT INTO stemformatics.configs (ref_type,ref_id) values('StemformaticsQueue','/var/www/pylons-data/prod/jobs/StemformaticsQueue/');
INSERT INTO stemformatics.configs (ref_type,ref_id) values('DatasetGCTFiles','/var/www/pylons-data/prod/GCTFiles/');
INSERT INTO stemformatics.configs (ref_type,ref_id) values('DatasetCLSFiles','/var/www/pylons-data/prod/CLSFiles/');
INSERT INTO stemformatics.configs (ref_type,ref_id) values('GPQueue','/var/www/pylons-data/prod/jobs/GPQueue/');

Update stemformatics.configs set ref_id = 60 where ref_type = 'mouse_db';
Update stemformatics.configs set ref_id = 59 where ref_type = 'human_db';
