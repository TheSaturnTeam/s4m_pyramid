update annotation_databases set annotation_version = 'Gene 69' where  an_database_id = 56;
update annotation_databases set annotation_version = 'Gene 67' where  an_database_id = 46;
insert into annotation_databases (an_database_id, genome_version, annotator, annotation_version,model_id) values (57,'Homo sapiens','Ensembl','Gene 86','Human');
insert into annotation_databases (an_database_id, genome_version, annotator, annotation_version,model_id) values (58,'Mus musculus','Ensembl','Gene 86','Mouse');
insert into annotation_databases (an_database_id, genome_version, annotator, annotation_version,model_id) values (59,'Homo sapiens','Ensembl','Gene 91','Human');
insert into annotation_databases (an_database_id, genome_version, annotator, annotation_version,model_id) values (60,'Mus musculus','Ensembl','Gene 91','Mouse');

create table stemformatics.ensembl_versions_gene_mappings(from_id text, to_id text, from_ensembl_version integer, to_ensembl_version integer,from_db_id integer,to_db_id integer, constraint ensemble_versions_gene_mappings_pkey PRIMARY KEY (from_id,to_id,from_ensembl_version,to_ensembl_version,from_db_id,to_db_id)); 

\copy stemformatics.ensembl_versions_gene_mappings from '/home/isha/main.csv' with CSV header
